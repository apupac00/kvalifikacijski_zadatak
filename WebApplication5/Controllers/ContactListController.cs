﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class ContactListController : ApiController
    {
        public HttpResponseMessage Get()
        {
            string query = @"
                     select PersonID, FirstName, LastName, Address, PhoneNumber from
                     dbo.ContactList                  
                     ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ContactListAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);

        }
        public string Post(ContactList cont)
        {
            try
            {
                string query = @"
                insert into dbo.ContactList values
                (
                 ' " + cont.FirstName + @"'
                 ,' " + cont.LastName + @"'
                 ,' " + cont.Address + @"'
                 ,' " + cont.PhoneNumber + @"'
                 ,' " + cont.Email + @"'
                 )
                ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ContactListAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Added Successfully!!";
            }
            catch (Exception)
            {
                return "Faild to Add!!";
            }
        }
        public string Put(ContactList cont)
        {
            try
            {
                string query = @"
                update dbo.ContactList set 
                FirstName=' " + cont.FirstName + @"'
                ,LastName=' " + cont.LastName + @"'
                ,Address=' " + cont.Address + @"'
                ,PhoneNumber=' " + cont.PhoneNumber + @"'
                ,Email=' " + cont.Email + @"'
                where PersonID=" + cont.PersonID + @"
                ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ContactListAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Updated Successfully!!";
            }
            catch (Exception)
            {
                return "Faild to Update!!";
            }
        }

        public string Delete(int id)
        {
            try
            {
                string query = @"
                delete from  dbo.ContactList 
                where PersonID=" + id + @"
                ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ContactListAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Deleted Successfully!!";
            }
            catch (Exception)
            {
                return "Faild to Delete!!";
            }
        }
        [Route("api/ContactList/GetAllContactNames")]
        [HttpGet]
        public HttpResponseMessage GetContactNames()
        {

            string query = @"
                select firstName from dbo.ContactList";

            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["ContactListAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }

            return Request.CreateResponse(HttpStatusCode.OK, table);

        }

    }
}
